---
title: 'Useful Linux Commands'
taxonomy:
    category: docs
visible: true
---

### Ghostscript ([Link](https://www.ghostscript.com/))
#### RGB to CMYK Conversion
This command will convert PDFs in the RGB color space, such as those created in Inkscape, to CMYK for print. Within the terminal navigate to the file directory and replace **out.pdf** with the desired output CMYK file name and **in.pdf** with the existing RGB file:

`gs -o out.pdf -sDEVICE=pdfwrite -dUseCIEColor -sProcessColorModel=DeviceCMYK -sColorConversionStrategy=CMYK -dPDFSETTINGS=/prepress -dEncodeColorImages=false -sColorConversionStrategyForImages=CMYK in.pdf`

#### Merge PDF Files
This command will merge two PDF files and reduce the dpi to 300. This is useful when generating PDFs in Inkscape.

`gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=out.pdf in1.pdf in2.pdf`